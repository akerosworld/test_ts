interface PokeApi {
    count: number,
    next: string,
    previous: string,
    results: PokeThings[]
}

interface PokeThings {
    name: string,
    url: string
}

export default defineEventHandler(async () => {
    const data: PokeApi = await  $fetch( 'https://pokeapi.co/api/v2/pokemon/?limit=20&offset=20')
    return {data}
})