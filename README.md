# Nuxt 3 Minimal Starter

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Setup

Make sure to install the dependencies:

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install
```

## Development Server

Start the development server on http://localhost:3000

```bash
npm run dev
```


# Réalisation test technique :

1. Dans la page index, compléter la liste des pages (books, count, poke, users)
2. Dans la page poke, afficher 20 pokemons sous la forme de cartes avec le framework CSS Vuetify 3 déja installé sur ce projet (les appels API sont préconfigurés dans le dossier server/api)
3. Ajouter un filtre sur le type du pokemon (eau, feu, psy... )
4. Ajouter un système de pagination pour afficher les 20 pokemons suivants ou précédents
5. (Bonus) Ajouter un pokemon dans une équipe en cliquant sur une carte et afficher l'équipe dans la page (visuel à votre appréciation), limiter la taille de l'équipe à 6 pokemons (programmation orientée objet attendue)

Documentation api => https://pokeapi.co/docs/v2#pokemon
Vuetify 3 => https://vuetifyjs.com/en/getting-started/installation/